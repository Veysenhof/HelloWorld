import java.util.Scanner;

public class Excercise5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        double price = 0.0;
        double instalment = 0.0;

        System.out.println("If you want to buy the product, please insert the price: ");
        price = input.nextDouble();
        System.out.println("And now insert number of instalments: ");
        instalment = input.nextDouble();
        while (instalment < 6 && instalment > 48) {
            System.out.println("I'm sorry, but you must choose number of instalments from 6 to 48. Instert number again: ");
            price = input.nextDouble();
        }

        if (instalment >= 6 && instalment <= 12) {
            System.out.println("Your monthly instalment is " + ((price * 0.025) + price) / instalment);
        }
        if (instalment >= 13 && instalment <= 24) {
            System.out.println("Your monthly instalment is " + ((price * 0.050) + price) / instalment);
        }
        if (instalment >= 25 && instalment <= 48) {
            System.out.println("Your monthly instalment is " + ((price * 0.100) + price) / instalment);
        }
    }
}